jQuery(document).ready(function (){
    jQuery(".mobile-menu-trigger").on('click', function () {
        jQuery("body").toggleClass("mobile-menu-activated");
        jQuery(".mobile-menu-wrapper ul").toggle();
    });
})